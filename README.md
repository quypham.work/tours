# Tours UI from Figma

## Project Setup

npm install. 
npm start

## Notes for UI

### Manage Profile

http://localhost:3000/manage_profile/

### Manage Tour

http://localhost:3000/manage_tour/

### Create Tour

http://localhost:3000/create_tour/

### Upload Tour

http://localhost:3000/upload_tour/
