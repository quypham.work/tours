import React from "react";
import { connect } from "react-redux";
import { addItems } from "../actions";

const UploadTour = ({ qty, dispatch }) => {
  let input, item;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!input.value.trim()) {
      return;
    }
    item = {
      text: input.value,
    };
    dispatch(addItems(item));
    input.value = "";
  };

  return (
    <div className={"wrapper"}>
      <div class="profile">
        <span class="profile_name">Hồ sơ của tôi</span>
        <div class="profile_info">
          <span class="profile_info_text">Thông tin cơ bản</span>
          <div class="profile_form">
            <div class="profile_form_name">
              <span class="profile_form_name_hoten">Họ tên</span>
              <span class="profile_form_name_hoten_content">Maria Glover</span>

            </div>
            <div class="profile_form_guide">
              <div class="profile_form_guide_name">
                <div class="profile_form_guide_name_1"></div>
                <span class="profile_form_guide_name_luulai">Lưu lại</span>
              </div>
              <span class="profile_form_guide_name_ho">Họ của bạn</span>
              <span class="profile_form_guide_name_ten">Tên của bạn</span>
              <div class="profile_form_guide_name_detail_1">
                <div class="profile_form_guide_name_detail_1a"></div>
                <span class="profile_form_guide_name_detail_1b">Glover</span>
              </div>
              <div class="profile_form_guide_name_detail_2a">
                <div class="profile_form_guide_name_detail_2b"></div>
                <span class="profile_form_guide_name_detail_2c">Maria</span>
              </div>
            </div>
          </div>
          <div class="profile_form_guide_date">
            <span class="profile_form_guide_date_ngay">Ngày, tháng, năm sinh</span>
            <span class="profile_form_guide_date_detail">14/08/2000</span>
          </div>
          <div class="profile_form_guide_email">

            <span class="profile_form_guide_email_name">Email</span>
            <span class="profile_form_guide_email_content">maria.tourguide@gmail.com</span>

          </div>
          <div class="profile_form_guide_phone">

            <span class="profile_form_guide_phone_1">Số điện thoại</span>
            <span class="profile_form_guide_phone_2">+61 491 570 156</span>

          </div>
          <div class="profile_form_guide_nam">

            <span class="profile_form_guide_nam_1">Số năm kinh nghiệm</span>
            <span class="profile_form_guide_nam_2">5 năm</span>
          </div>
          <div class="profile_form_guide_khuvuc">
            <span class="profile_form_guide_khuvuc_1">Khu vực phục vụ</span>
            <span class="profile_form_guide_khuvuc_2">Sydney</span>
          </div>
          <div class="profile_form_guide_khuvuc_3">
            <span class="profile_form_guide_khuvuc_31">Khu vực phục vụ</span>
            <span class="profile_form_guide_khuvuc_32">Trọn gói · Đi về trong ngày</span>
          </div>
          <div class="profile_form_guide_chuyenmon">
            <span class="profile_form_guide_chuyenmon_1">Chuyên môn về tour</span>
            <span class="profile_form_guide_chuyenmon_2">
              Tham quan ngắm cảnh · Ẩm thực · Mạo hiểm ngoài trời
            </span>
          </div>
        </div>
        <div class="profile_form_guide_thongtinkhac">
          <span class="profile_form_guide_thongtinkhac_1">Thông tin khác</span>
          <div class="profile_form_guide_thongtinkhac_2">
            <span class="profile_form_guide_thongtinkhac_21">Chi phí mỗi giờ</span>
            <span class="profile_form_guide_thongtinkhac_22">$30 - 39</span>
          </div>
          <div class="profile_form_guide_thongtinkhac_3">
            <span class="profile_form_guide_thongtinkhac_31">Full Australian Driver Licence</span>
          </div>
          <div class="profile_form_guide_thongtinkhac_4">
            <span class="profile_form_guide_thongtinkhac_41">Valid Police Clearance</span>
            <span class="profile_form_guide_thongtinkhac_42" />
          </div>
          <div class="profile_form_guide_thongtinkhac_valid">
            <span class="profile_form_guide_thongtinkhac_valid_1">Valid Working with Children</span>
          </div>
          <div class="profile_form_guide_thongtinkhac_certificate">
            <span class="profile_form_guide_thongtinkhac_certificate_1">Valid Firstaid Certificate</span>
          </div>
          <div class="profile_form_guide_self">
            <span class="profile_form_guide_self_1">Self Intro</span>
            <span class="profile_form_guide_self_2">
              Hello 👋 my name is Maria (24 years old) and am Russian, but ...
            </span>
          </div>
        </div>
        <div class="profile_form_guide_self_3">
          <div class="profile_form_guide_self_3a">
            <div class="profile_form_guide_self_3b">
              <span class="profile_form_guide_self_3b1">Xin chào, Maria!</span>
              <span class="profile_form_guide_self_3b2">
                Bạn đã hoàn thành 75% hồ sơ, Hãy tiếp tục!
              </span>
              <div class="profile_form_guide_self_3b3">
                <div class="profile_form_guide_self_3b4">
                  <span class="profile_form_guide_self_3b5">Tiến độ hoàn thành hồ sơ</span>
                  <span class="profile_form_guide_self_3b6">78%</span>
                </div>
              </div>
            </div>
          </div>
          <div class="profile_form_guide_self_3b7">
            <span class="profile_form_guide_self_3b8">Quản lí tài khoản</span>
            <div class="profile_form_guide_self_3b9">
              <span class="profile_form_guide_self_3b10">Cập nhật hồ sơ</span>
            </div>
            <div class="profile_form_guide_self_3b11">
              <span class="profile_form_guide_self_3b12">Đổi mật khẩu</span>
            </div>
            <div class="profile_form_dangxuat">
              <span class="profile_form_dangxuat_1">Đăng xuất</span>
              <div class="profile_form_dangxuat_2">
                <div class="profile_form_dangxuat_3"></div>
                <span class="profile_form_dangxuat_4">4</span>
              </div>
              <div class="profile_form_dangxuat_5">
              </div>
            </div>
          </div>
          <div class="profile_form_yeucau_congdong">
            <span class="profile_form_yeucau_congdong_1">Yêu cầu cộng đồng</span>
            <div class="profile_form_yeucau_congdong_2">
              <div class="profile_form_yeucau_congdong_3"></div>
              <span class="profile_form_yeucau_congdong_4">4</span>
            </div>
          </div>
          <div class="profile_form_quanlytour">
            <div class="profile_form_quanlytour_1"></div>
            <span class="profile_form_quanlytour_2">4</span>
          </div>
          <div class="profile_form_quanlytour_3">
            <span class="profile_form_quanlytour_4">Quản lí Tour</span>
            <div class="profile_form_quanlytour_5">
              <span class="profile_form_quanlytour_6">Thêm Tour</span>
              <div class="profile_form_quanlytour_7">
                <div class="profile_form_quanlytour_8"></div>
                <span class="profile_form_quanlytour_9">4</span>
              </div>
            </div>
            <div class="profile_form_quanlytour_10">
              <span class="profile_form_quanlytour_11">Quản lí Tour</span>
              <div class="profile_form_quanlytour_12">
                <div class="profile_form_quanlytour_13"></div>
                <span class="profile_form_quanlytour_14">4</span>
              </div>
            </div>
          </div>
          <div class="profile_form_quanlytour_15">
            <span class="profile_form_quanlytour_16">Quản lí bài viết</span>
            <div class="profile_form_quanlytour_17">
              <span class="profile_form_quanlytour_18">Thêm bài viết</span>
              <div class="profile_form_quanlytour_19">
                <div class="profile_form_quanlytour_20"></div>
                <span class="profile_form_quanlytour_21">4</span>
              </div>
            </div>
            <div class="profile_form_quanlytour_22">
              <span class="profile_form_quanlytour_23">Quản lí bài viết</span>
              <div class="profile_form_quanlytour_24">
                <div class="profile_form_quanlytour_25"></div>
                <span class="profile_form_quanlytour_26">4</span>
              </div>
            </div>
          </div>
        </div>
        <div class="profile_form_quanlytour_27">
          <div class="profile_form_quanlytour_28"></div>
          <div class="profile_form_quanlytour_29">
            <span class="profile_form_quanlytour_30">5starguides</span>
          </div>
          <div class="profile_form_quanlytour_31">
            <div class="profile_form_quanlytour_32">
              <span class="profile_form_quanlytour_33">Khám phá</span>
              <div class="profile_form_quanlytour_34">
              </div>
            </div>
            <span class="about_1">Về chúng tôi</span>
            <span class="about_2">Blog</span>
            <span class="about_3">Đăng nhập</span>
          </div>
          <div class="about_4">
            <div class="about_5"></div>
            <span class="about_6">Bất cứ đâu - Trải nghiệm</span>
          </div>
          <div class="about_7">
            <div class="about_8"></div>
            <span class="about_9">Tùng Phan</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect()(UploadTour);
