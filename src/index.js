import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './components/App';
import ManageProfile from './manage_profile/ManageProfile';
import ManageTour from './manage_tour/ManageTour';
import CreateTour from './create_tour/CreateTour';
import UploadTour from './upload_tour/UploadTour';
import rootReducer from './reducers';
import ItemsList from './components/ItemsList';
import './index.css';
import './manage_tour/css/main.css';
import './create_tour/css/main.css';
import './upload_tour/css/main.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

const store = createStore(rootReducer);

ReactDOM.render(
        <Provider store={ store }>
            <Router>
                <Switch>
                    <Router path='/manage_profile'>
                        <ManageProfile/>
                    </Router>
                    <Router path='/manage_tour'>
                        <ManageTour/>
                    </Router>
                    <Router path='/create_tour'>
                        <CreateTour/>
                    </Router>
                    <Router path='/upload_tour'>
                        <UploadTour/>
                    </Router>
                </Switch>
            </Router>
            <hr/>
        </Provider>,
    document.getElementById('root')
);