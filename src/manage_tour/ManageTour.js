import React from "react";
import { connect } from "react-redux";
import { addItems } from "../actions";

const ManageTour = ({ qty, dispatch }) => {
  let input, item;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!input.value.trim()) {
      return;
    }
    item = {
      text: input.value,
    };
    dispatch(addItems(item));
    input.value = "";
  };

  return (
    <div className={"wrapper"}>
      <div class="quanlytour_main">
        <span class="quanlytour_main_name">Quản lí Tour</span>
        <div class="name"></div>
        <div class="quanlytour_main_1">
          <div class="quanlytour_main_2">
            <span class="quanlytour_main_3">
              Thưởng thức xì gà, hút xì gà đúng điệu, lắng đọng và trò chuyện
            </span>
            <div class="quanlytour_main_4">
              <div class="quanlytour_main_5">
                <span class="quanlytour_main_6">Maria</span>
              </div>
              <span class="quanlytour_main_7">2 giờ</span>
            </div>
            <div class="name"></div>
            <div class="quanlytour_main_8">
              <span class="quanlytour_main_9">$20 / người</span>
            </div>
          </div>
          <div class="quanlytour_main_10">
            <div class="quanlytour_main_11">
              <div class="quanlytour_main_12">
                <span class="quanlytour_main_13">Xóa Tour</span>
              </div>
            </div>
            <div class="name"></div>
            <div class="quanlytour_edit">
              <div class="quanlytour_edit_1">
                <span class="quanlytour_edit_2">Chỉnh sửa</span>
              </div>
            </div>
            <div class="name"></div>
          </div>
        </div>
        <div class="quanlytour_edit_3">
          <div class="quanlytour_edit_4">
            <span class="quanlytour_edit_6">
              Tập Muay Thái cùng Trí trên sân thượng tươi mát đầy cây xanh
            </span>
            <div class="quanlytour_edit_7">
              <div class="quanlytour_edit_8">
                <div class="quanlytour_edit_9"></div>
                <span class="quanlytour_edit_10">Maria</span>
              </div>
              <span class="quanlytour_edit_11">2 giờ</span>
            </div>
            <div class="name"></div>
            <div class="quanlytour_edit_12">
              <span class="quanlytour_edit_13">$20 / người</span>
            </div>
          </div>
          <div class="quanlytour_edit_14">
            <div class="quanlytour_edit_15">
              <div class="quanlytour_edit_16">
                <span class="quanlytour_edit_17">Xóa Tour</span>

              </div>
            </div>
            <div class="name"></div>
            <div class="quanlytour_edit_18">
              <div class="quanlytour_edit_19">
                <span class="quanlytour_edit_20">Chỉnh sửa</span>
              </div>
            </div>
            <div class="name"></div>
          </div>
        </div>
        <div class="quanlytour_content">
          <div class="quanlytour_content_1">
            <span class="quanlytour_content_2">Tìm hiểu về nghệ thuật Vẽ</span>
            <div class="quanlytour_content_3">
              <div class="quanlytour_content_4">
                <div class="quanlytour_content_5"></div>
                <span class="quanlytour_content_6">Maria</span>
              </div>
              <span class="quanlytour_content_7">2 giờ</span>
            </div>
            <div class="name"></div>
            <div class="quanlytour_content_8">
              <span class="quanlytour_content_9">$20 / người</span>
            </div>
          </div>
          <div class="quanlytour_content_10">
            <div class="quanlytour_content_11">
              <div class="quanlytour_content_12">
                <span class="quanlytour_content_13">Xóa Tour</span>
              </div>
            </div>
            <div class="name"></div>
            <div class="quanlytour_content_edit">
              <div class="quanlytour_content_edit_1">
                <span class="quanlytour_content_edit_2">Chỉnh sửa</span>
              </div>
            </div>
            <div class="name"></div>
          </div>
        </div>
        <div class="quanlytour_content_edit_3">
          <div class="quanlytour_content_edit_4">
            <div class="quanlytour_content_edit_5">
              <span class="quanlytour_content_edit_6">Xin chào, Maria!</span>
              <span class="quanlytour_content_edit_7">
                Bạn đã hoàn thành 75% hồ sơ, Hãy tiếp tục!
              </span>
              <div class="quanlytour_content_edit_8">
                <div class="quanlytour_content_edit_9">
                  <span class="quanlytour_content_edit_10">Tiến độ hoàn thành hồ sơ</span>
                  <span class="quanlytour_content_edit_11">78%</span>
                </div>
              </div>
            </div>
          </div>
          <div class="quanlytour_content_change">
            <span class="quanlytour_content_change_1">Quản lí tài khoản</span>
            <div class="quanlytour_content_change_2">
              <span class="quanlytour_content_change_3">Cập nhật hồ sơ</span>
            </div>
            <div class="quanlytour_content_change_4">
              <span class="quanlytour_content_change_5">Đổi mật khẩu</span>
            </div>
            <div class="quanlytour_content_change_6">
              <span class="quanlytour_content_change_7">Đăng xuất</span>
            </div>
          </div>
          <div class="quanlytour_content_yeucaucongdong">
            <span class="quanlytour_content_yeucaucongdong_1">Yêu cầu cộng đồng</span>
          </div>
          <div class="quanlytour_content_yeucaucongdong_2">
            <span class="quanlytour_content_yeucaucongdong_3">Quản lí Tour</span>
            <div class="quanlytour_content_yeucaucongdong_4">
              <span class="quanlytour_content_yeucaucongdong_5">Thêm Tour</span>
            </div>
            <div class="quanlytour_content_yeucaucongdong_6">
              <span class="quanlytour_content_yeucaucongdong_7">Quản lí Tour</span>
            </div>
          </div>
          <div class="quanlytour_content_yeucaucongdong_8">
            <span class="quanlytour_content_yeucaucongdong_9">Quản lí bài viết</span>
            <div class="quanlytour_content_yeucaucongdong_10">
              <span class="quanlytour_content_yeucaucongdong_11">Thêm bài viết</span>
            </div>
            <div class="quanlytour_content_yeucaucongdong_12">
              <span class="quanlytour_content_quanlybaiviet">Quản lí bài viết</span>
            </div>
          </div>
        </div>
        <div class="quanlytour_content_quanlybaiviet_1">
          <div class="quanlytour_content_quanlybaiviet_2">
            <span class="quanlytour_content_quanlybaiviet_3">5starguides</span>
          </div>
          <div class="quanlytour_content_quanlybaiviet_4">
            <div class="quanlytour_content_quanlybaiviet_5">
              <span class="quanlytour_content_quanlybaiviet_6">Khám phá</span>
            </div>
            <span class="quanlytour_content_quanlybaiviet_7">Về chúng tôi</span>
            <span class="quanlytour_content_quanlybaiviet_8">Blog</span>
            <span class="quanlytour_content_quanlybaiviet_dangnhap">Đăng nhập</span>
          </div>
          <div class="quanlytour_content_quanlybaiviet_header">
            <span class="quanlytour_content_quanlybaiviet_header_1">Bất cứ đâu - Trải nghiệm</span>
          </div>
          <div class="quanlytour_content_quanlybaiviet_header_author">
            <span class="quanlytour_content_quanlybaiviet_header_author_1">Tùng Phan</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect()(ManageTour);
