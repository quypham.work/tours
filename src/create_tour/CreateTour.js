import React from "react";
import { connect } from "react-redux";
import { addItems } from "../actions";

const CreateTour = ({ qty, dispatch }) => {
  let input, item;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!input.value.trim()) {
      return;
    }
    item = {
      text: input.value,
    };
    dispatch(addItems(item));
    input.value = "";
  };

  return (
    <div className={"wrapper"}>
      <div class="add-tour-form">
        <span class="add-tour-form-name">Thêm Tour</span>
        <div class="add-tour-form-name-1">
          <div class="add-tour-form-name-2"></div>
          <div class="add-tour-form-name-3">
            <span class="add-tour-form-name-4">Tên của Tour ?</span>
            <div class="add-tour-form-name-5">
              <span class="add-tour-form-name-6">
                Thủy phi cơ ngắm toàn cảnh Sydney 30 phút
              </span>
            </div>
          </div>
          <div class="add-tour-form-name-loai">
            <span class="add-tour-form-name-loai-1">Loại hình của Tour ?</span>
            <div class="add-tour-form-name-loai-2">
              <span class="add-tour-form-name-loai-3">Về trong ngày</span>
            </div>
            <div class="add-tour-form-name-loai-4">
              <span class="add-tour-form-name-loai-5">Trọn gói</span>
            </div>
          </div>
          <div class="add-tour-form-name-loai-6">
            <div class="add-tour-form-name-loai-7">
              <div class="add-tour-form-name-loai-8">
                <span class="add-tour-form-name-loai-9">Tham quan ngắm cảnh</span>
              </div>
              <div class="add-tour-form-name-loai-10">
                <span class="add-tour-form-name-loai-11">Mạo hiểm ngoài trời</span>
              </div>
              <div class="add-tour-form-name-loai-12">
                <span class="add-tour-form-name-loai-13">Văn hóa và Giáo dục</span>
              </div>
              <div class="add-tour-form-name-loai-14">
                <span class="add-tour-form-name-loai-15">Kinh doanh Đầu tư</span>
              </div>
              <div class="add-tour-form-name-content-1">
                <span class="add-tour-form-name-content-2">Ẩm thực</span>
              </div>
              <div class="add-tour-form-name-content-3">
                <span class="add-tour-form-name-content-4">Trải nghiệm Nông trại và Vùng sâu</span>
              </div>
            </div>
            <span class="add-tour-form-name-content-5">Những hoạt động trải nghiệm ?</span>
          </div>
          <div class="add-tour-form-name-content-6">
            <div class="add-tour-form-name-content-7">
              <span class="add-tour-form-name-content-8">Chi phí</span>
              <span class="add-tour-form-name-content-9">Chi phí mỗi giờ dành cho 1 người</span>
            </div>
            <div class="add-tour-form-name-content-10">
              <div class="add-tour-form-name-content-11">
                <span class="add-tour-form-name-content-12">Free</span>
                <span class="add-tour-form-price-1">$10</span>
                <span class="add-tour-form-price-2">$30</span>
                <span class="add-tour-form-price-3">$50</span>
              </div>
            </div>
          </div>
          <div class="add-tour-form-picture-01">
            <span class="add-tour-form-picture-02">Hình ảnh về Tour (2/7)</span>
            <span class="add-tour-form-picture-03">Yêu cầu Upload tối thiểu 7 hình ảnh</span>
          </div>
          <div class="add-tour-form-description-01">
            <span class="add-tour-form-description-02">Du khách sẽ được trải nghiệm những gì ?</span>
            <div class="add-tour-form-description-03">
              <div class="name"></div>
              <span class="add-tour-form-description-04">
                Experience a true offgrid experience with otherworldly
                bioluminescence in an ocean bay off the coast of Halfmoon Bay,
                British Columbia. Welcome nature lovers, we are an eco-friendly,
                off grid, solar powered Rv at sea. We use a top of the line
                organic composting toilet system to keep nature at its best.
              </span>
            </div>
          </div>
          <div class="add-tour-form-description-05">
            <span class="add-tour-form-description-06">
              Hành trình trải nghiệm của Du khách như thế nào ?{" "}
            </span>
            <div class="name"></div>
            <div class="add-tour-form-description-07">
              <div class="add-tour-form-description-08">
                <div class="add-tour-form-description-09">
                  <div class="add-tour-form-description-10">
                    <span class="add-tour-form-description-11">
                      We will go from the dock or beach, on a 5 min boat ride to
                      your stay in the bay.
                    </span>
                  </div>
                  <div class="add-tour-form-description-12">
                    <span class="add-tour-form-description-13">Overnight stay on a yacht</span>
                  </div>
                </div>
                <div class="add-tour-form-description-14">
                  <span class="add-tour-form-description-15">Hình ảnh minh họa</span>
                </div>
              </div>
              <span class="add-tour-form-description-16">Hoạt động ngày 2</span>
            </div>
            <div class="add-tour-form-description-17">
              <span class="add-tour-form-description-18">Thêm hoạt động</span>
            </div>
          </div>
        </div>
        <div class="save-tour-form">
          <span class="save-tour-form-text">Lưu Tour</span>
        </div>
        <div class="save-tour-form-text-1">
          <div class="save-tour-form-text-2">
            <div class="save-tour-form-text-3">
              <span class="save-tour-form-text-4">Xin chào, Maria!</span>
              <span class="save-tour-form-text-5">
                Bạn đã hoàn thành 75% hồ sơ, Hãy tiếp tục!
              </span>
              <div class="save-tour-form-text-6">
                <div class="save-tour-form-text-7">
                  <span class="save-tour-form-text-8">Tiến độ hoàn thành hồ sơ</span>
                  <span class="save-tour-form-text-9">78%</span>
                </div>
              </div>
            </div>
          </div>
          <div class="save-tour-form-text-quanly">
            <span class="save-tour-form-text-quanly-name">Quản lí tài khoản</span>
            <div class="save-tour-form-text-quanly-name-01">
              <span class="save-tour-form-text-quanly-name-02">Cập nhật hồ sơ</span>
            </div>
            <div class="save-tour-form-text-quanly-name-03">
              <span class="save-tour-form-text-quanly-name-04">Đổi mật khẩu</span>
            </div>
            <div class="save-tour-form-text-quanly-name-05">
              <span class="save-tour-form-text-quanly-name-06">Đăng xuất</span>
            </div>
          </div>
          <div class="save-tour-form-text-quanly-name-07">
            <span class="save-tour-form-text-quanly-name-08">Yêu cầu cộng đồng</span>
          </div>
          <div class="save-tour-form-text-quanly-name-09">
            <span class="save-tour-form-text-quanly-name-10">Quản lí Tour</span>
            <div class="add-tour-text-quanly-name-new-form">
              <span class="add-tour-text-quanly-name-new-form-1">Thêm Tour</span>
            </div>
            <div class="add-tour-text-quanly-name-new-form-2">
              <span class="add-tour-text-quanly-name-new-form-3">Quản lí Tour</span>
            </div>
          </div>
          <div class="add-tour-text-quanly-name-new-form-4">
            <span class="add-tour-text-quanly-name-new-form-5">Quản lí bài viết</span>
            <div class="add-tour-text-quanly-name-new-form-6">
              <span class="add-tour-text-quanly-name-new-form-7">Thêm bài viết</span>
            </div>
            <div class="add-tour-text-quanly-name-new-form-8">
              <span class="add-tour-text-quanly-name-new-form-9">Quản lí bài viết</span>
            </div>
          </div>
        </div>
        <div class="add-tour-text-quanly-name-new-form-10">
          <div class="add-tour-text-quanly-name-new-form-11">
            <span class="add-tour-text-quanly-name-new-form-12">5starguides</span>
          </div>
          <div class="add-tour-text-quanly-name-new-form-13">
            <div class="text-add-tour-form">
              <span class="text-add-tour-form-01">Khám phá</span>
            </div>
            <span class="text-add-tour-form-02">Về chúng tôi</span>
            <span class="text-add-tour-form-03">Blog</span>
            <span class="text-add-tour-form-04">Đăng nhập</span>
          </div>
          <div class="text-add-tour-form-05">
            <span class="text-add-tour-form-06">Bất cứ đâu - Trải nghiệm</span>
            <div class="text-add-tour-form-07">
              <div class="name"></div>
              <div class="name"></div>
            </div>
          </div>
          <div class="text-add-tour-form-08">
            <span class="text-add-tour-form-09">Tùng Phan</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect()(CreateTour);
